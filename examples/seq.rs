use arrayvec::ArrayVec;
use qpr_hash::QprMapper;
use rayon::prelude::*;
use std::convert::TryFrom;

use xorshift::xoroshiro128::Xoroshiro128;
use xorshift::{Rng, SeedableRng};

#[derive(Clone, Copy, Debug, Default)]
struct Event {
    id: u32,
    order: u16,
}

#[derive(Clone, Copy, Debug, Default)]
struct Projection {
    target: u32,
}

const PER_OBJECT_EVENT_COUNT_MAX: u64 = 1024;

struct SyncUnsafeCell<T> {
    inner: std::cell::UnsafeCell<T>,
}

unsafe impl Sync for SyncUnsafeCell<Vec<Event>> {}

fn main() {
    let lim = 1_000_000_000;
    let mapper = QprMapper::new(0, lim - 1);

    println!("Generating Random sequence");
    let start = std::time::Instant::now();
    let mut rng = Xoroshiro128::from_seed(&[0, 1]);

    let (id_start_range, _) =
        std::iter::repeat_with(|| rng.gen_range(1, PER_OBJECT_EVENT_COUNT_MAX))
            .enumerate()
            .scan(0, |current, (idx, r)| {
                if *current > lim {
                    None
                } else {
                    *current = *current + r;
                    Some((idx, r))
                }
            })
            .fold(
                (
                    Vec::<(u32, u32, u16)>::with_capacity(
                        (lim / (PER_OBJECT_EVENT_COUNT_MAX / 2)) as usize,
                    ),
                    0,
                ),
                |(mut ret, current), (idx, r)| {
                    let rn = current + r;
                    let mut rt: usize = r as usize;
                    if rn > lim {
                        rt = rt - (rn as usize - lim as usize)
                    }
                    ret.push((idx as u32, current as u32, rt as u16));
                    (ret, current + r)
                },
            );
    let acc = SyncUnsafeCell {
        inner: std::cell::UnsafeCell::new(vec![Event { id: 0, order: 0 }; lim as usize]),
    };
    println!("{}", id_start_range.len());
    println!("elapsed {:?}", start.elapsed());

    let events_start = std::time::Instant::now();
    println!("Building Events");
    let _ = id_start_range.par_iter().for_each(|(idx, start, range)| {
        let mut projections: ArrayVec<[_; PER_OBJECT_EVENT_COUNT_MAX as usize]> = ArrayVec::new();
        projections.extend((*start..(*range as u32 + *start)).map(|j| Projection {
            target: mapper.hash(j as u64).unwrap() as u32,
        }));
        projections.sort_unstable_by(|x, y| std::cmp::Ord::cmp(&x.target, &y.target));
        projections.iter().enumerate().for_each(|(order, p)| {
            let r = acc.inner.get();
            unsafe {
                (*r)[p.target as usize] = Event {
                    id: *idx as u32,
                    order: u16::try_from(order).unwrap(),
                }
            };
        });
    });
    let ret = acc.inner.into_inner();
    // println!("Sorting in projected space");
    // ret.par_sort_unstable_by(|x, y| std::cmp::Ord::cmp(&(x.1).target, &(y.1).target));

    println!("elapsed {:?}", events_start.elapsed()); // note :?
    println!("Total elapsed {:?}", start.elapsed()); // note :?

    println!("{}", ret.len());
    println!("{:?}", ret[0]);
    // Sort Array by position in projected space

    println!("Checking");
    fn is_sorted<T>(data: &[T]) -> bool
    where
        T: Ord,
    {
        data.windows(2).all(|w| w[0] <= w[1])
    }

    // use std::collections::HashSet;

    // let mut target_set: HashSet<u32> = HashSet::new();

    // println!("Asserting Uniqueness");
    // ret.iter().for_each(|(_, p)| {
    //     assert!(!target_set.contains(&p.target));
    //     target_set.insert(p.target);
    // });

    println!("Grouping");
    use std::collections::HashMap;
    let mut grouped: HashMap<u32, Vec<u16>> = HashMap::new();
    for e in ret {
        grouped.entry(e.id).or_insert_with(|| vec![]).push(e.order);
    }

    // grouped.iter().for_each(|g| {
    //     println!("{:?}", g);
    // });
    println!("Asserting all are sorted");
    assert!(grouped.values().all(|es| is_sorted(&es)))
}

// Events can be groupded by idx,
