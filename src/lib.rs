use derive_more::{Display, Error};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CorrectedQprMapper {
    seed: u64,
    intermediate: u64,
    limit: u128,
    prime: u64,
    correction: QprCorrection,
}

impl CorrectedQprMapper {
    #[inline(always)]
    pub fn seed(&self) -> u64 {
        self.seed
    }

    #[inline(always)]
    pub fn hash(&self, x: u64) -> Result<u64, OutOfRangeError> {
        if (x as u128) < self.limit {
            Ok(permute_qpr(
                self.prime,
                self.limit,
                permute_qpr(self.prime, self.limit, x) + self.intermediate ^ self.correction.0,
            ))
        } else {
            Err(OutOfRangeError {})
        }
    }

    /// limit is the _inclusive_ upper limit
    #[inline(always)]
    pub fn new(seed: u64, limit: u64, correction: QprCorrection) -> Self {
        let prime = largest_congruent_mod_prime_below_or_equal_to(limit, 3, 4).unwrap();

        let limit = limit as u128 + 1;
        assert!(limit > 0);
        let intermediate = (permute_qpr(
            QprMapper::DEFAULT_PRIME,
            QprMapper::DEFAULT_LIM,
            permute_qpr(QprMapper::DEFAULT_PRIME, QprMapper::DEFAULT_LIM, seed + 1) + 0x46790905,
        ) as u128
            % limit) as u64;

        CorrectedQprMapper {
            seed,
            intermediate,
            limit,
            prime,
            correction,
        }
    }
}
#[derive(Debug, Display, Error)]
pub struct OutOfRangeError;

#[inline(always)]
pub fn largest_congruent_mod_prime_below_or_equal_to(
    upper_bound: u64,
    a: u64,
    b: u64,
) -> Option<u64> {
    if let Some(prime) = (0..=upper_bound)
        .rev()
        .filter(|n| primal::is_prime(*n))
        .find(|n| (n - a) % b == 0)
    {
        assert!(prime > 0);
        Some(prime)
    } else {
        None
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct QprCorrection(u64);

fn get_bit_bias<F>(lim: u64, mapper: F) -> Vec<i64>
where
    F: Fn(u64) -> u64 + Sync,
{
    use bitvec::prelude::*;
    use rayon::prelude::*;

    let counts = (0..lim)
        .into_par_iter()
        .fold(
            || [[0u64; 64]; 2],
            |mut acc, i| {
                let hash = mapper(i as u64);
                let i_slice = BitSlice::<Local, u64>::from_element(&i);
                let h_slice = BitSlice::<Local, u64>::from_element(&hash);
                for (idx, (b, h)) in i_slice.iter().zip(h_slice).enumerate() {
                    if (*b == *h) == *b {
                        acc[*b as usize][idx] += 1;
                    }
                }
                acc
            },
        )
        .collect::<Vec<[[u64; 64]; 2]>>();
    let counts = counts.iter().fold([[0; 32]; 2], |mut acc, it| {
        {
            (0..2).for_each(|i| (0..acc[0].len()).for_each(|j| acc[i][j] += it[i][j]));
        }
        acc
    });

    counts[0]
        .iter()
        .zip(counts[1].iter())
        .map(|(a, b)| *a as i64 - *b as i64)
        .collect()
}

fn to_u64(slice: &[u8]) -> u64 {
    slice.iter().rev().fold(0, |acc, &b| acc * 2 + b as u64)
}

impl QprCorrection {
    pub fn for_lim_and_prime(lim: u64, prime: u64) -> Self {
        // For each bit location Need to work out the probability that a 1 will
        // come back as a 1 and a 0 will come back as a 0.
        // If any location shows a bias one way or the other it should be xor'd
        // with itself
        // same_bits_count: [u32; 32] = [0; 32];

        let differences = get_bit_bias(lim, |i| permute_qpr(prime, lim as u128 + 1, i as u64));

        let corrections: Vec<u8> = differences.iter().map(|n| (*n < 0) as u8).collect();

        Self(to_u64(&corrections))
    }
}

#[inline(always)]
fn permute_qpr(prime: u64, lim: u128, x: u64) -> u64 {
    let x = (x as u128 % lim) as u64;
    if x > prime {
        return x;
    }

    let residue = ((x as u128 * x as u128) % prime as u128) as u64;

    if x <= prime / 2 {
        residue
    } else {
        prime - residue
    }
}

pub struct QprMapper {
    mapper: CorrectedQprMapper,
}
impl QprMapper {
    const DEFAULT_LIM: u128 = std::u64::MAX as u128 + 1;
    const DEFAULT_PRIME: u64 = 4294967291;

    #[inline(always)]
    pub fn default() -> Self {
        let seed = 0xdeadbeef as u64;

        let intermediate = permute_qpr(
            QprMapper::DEFAULT_PRIME,
            QprMapper::DEFAULT_LIM,
            permute_qpr(QprMapper::DEFAULT_PRIME, QprMapper::DEFAULT_LIM, seed + 1) + 0x46790905,
        );

        Self {
            mapper: CorrectedQprMapper {
                seed,
                intermediate,
                limit: std::u64::MAX as u128,
                prime: QprMapper::DEFAULT_PRIME,
                correction: QprCorrection(0),
            },
        }
    }

    /// limit is the _inclusive_ upper limit
    #[inline(always)]
    pub fn new(seed: u64, limit: u64) -> Self {
        Self {
            mapper: CorrectedQprMapper::new(seed, limit, QprCorrection(0)),
        }
    }

    #[inline(always)]
    pub fn seed(&self) -> u64 {
        self.mapper.seed()
    }

    #[inline(always)]
    pub fn hash(&self, x: u64) -> Result<u64, OutOfRangeError> {
        self.mapper.hash(x)
    }
}

pub struct QprRng {
    mapper: QprMapper,
    index: u64,
}

impl QprRng {
    pub fn new(seed: u64) -> Self {
        let mapper = QprMapper::new(seed, std::u64::MAX);
        let index = permute_qpr(
            4294967291,
            QprMapper::DEFAULT_LIM,
            permute_qpr(4294967291, QprMapper::DEFAULT_LIM, seed + 1) + 0x46790905,
        );
        Self { mapper, index }
    }

    pub fn get_random(&mut self) -> u64 {
        let ret = self.mapper.hash(self.index).unwrap();
        self.index += 1;
        ret
    }
}

proptest::prelude::proptest! {
    #[test]
    fn test_qpr_hash_unique(lim in proptest::prelude::Just(100)) {
        use std::collections::HashMap;

        //fn test_qpr_hash_unique(lim in proptest::num::u32::ANY) {
        let lim = lim as u64;

        let mut seen: HashMap<u64, u64> = HashMap::new();

        let prime = largest_congruent_mod_prime_below_or_equal_to(lim, 3, 4).unwrap();

        for i in 0..=lim {
            let res = permute_qpr(prime, lim as u128 + 1, i);
            assert!(!seen.contains_key(&res));
            seen.insert(res, i);
        }
    }

    #[test]
    fn test_qpr_mapper_unique(lim in proptest::prelude::Just(100)) {
        use std::collections::HashMap;
        let lim = lim as u64;

        let mut seen: HashMap<u64, u64> = HashMap::new();

        let mapper = QprMapper::new(0, lim);

        for i in 0..=lim {
            let res = mapper.hash(i).unwrap();
            assert!(!seen.contains_key(&res));
            seen.insert(res, i);
        }
    }

    #[test]
    fn test_qpr_hash(x in (0..=10000000u64)) {
        let lim = 100;
        let hasher = QprMapper::new(0, lim);
        if let Ok(res) = hasher.hash(x) {
            assert!(res <= lim)
        };
    }

    #[test]
    fn test_distribtion(lim in (11 as u32..std::u16::MAX as u32) ) {
        let lim = lim as u64;

        // For each bit location Need to work out the probability that a 1 will
        // come back as a 1 and a 0 will come back as a 0.
        // If any location shows a bias one way or the other it should be xor'd
        // with itself
        // same_bits_count: [u32; 32] = [0; 32];

        let prime = largest_congruent_mod_prime_below_or_equal_to(lim, 3, 4).unwrap();
        let differences = get_bit_bias(lim, |i| permute_qpr(prime, lim as u128 + 1, i as u64));

        let corrections: Vec<u8> = differences.iter().map(|n| (*n < 0) as u8).collect();

        let corrected_mapper = CorrectedQprMapper::new(0, lim, QprCorrection(to_u64(&corrections)));
        let diff_after_correction = get_bit_bias(lim, |i| corrected_mapper.hash(i).unwrap());

        // Utterly arbitrary check that this doesn't ever make things too much worse.
        assert!(
            (differences.iter().fold(0, |a, x| a + x) + ((lim as f64).sqrt() as i64) * 14) > diff_after_correction.iter().fold(0, |a, x| a + x)
        );
    }
}
